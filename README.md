## 思路

其实可以只用 collectionView 做的，但是看着京东里面，商品那一边还有悬停效果，于是就考虑用 tableView + collectionView 做了。

collectionView 有2个 section，section0 是系统cell，然后添加 tableView。section1 是为你推荐商品啥的，正常的 collectionViewCell

首先加载tableView，获取到它加载完内容后的 contentSize。然后，赋值给collectionVIew的代理，将它用作section0的size，这样就可以滚动了。

这里有个问题，就是这样做之后，所有的tableView都被舒展开了，就没有悬停的效果了。还没想好怎么处理。

![demo 图](https://gitlab.com/zlanchun/JDShopCarDemo/raw/master/JDShoppingCarDemo.gif)
