//
//  ShoppingInfoCell.m
//  ShoppingCar
//
//  Created by z on 2017/7/11.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "ShoppingInfoCell.h"
#import "ShoppingInfoEditAmountView.h"
#import "ShoppingCarHeader.h"
#import "NSString+MoneyStyle.h"

@interface ShoppingInfoCell()
@property (nonatomic,strong) UIButton *markButton;
@property (nonatomic,strong) UIImageView *coverImgView;
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *subTitleLabel;
@property (nonatomic,strong) UILabel *priceLabel;
@property (nonatomic,strong) ShoppingInfoEditAmountView *amountView;
@end

@implementation ShoppingInfoCell

#pragma mark - life cycle

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupUI];
        self.titleLabel.text = @"【京东配送】好东西多多";
        self.subTitleLabel.text = @"全是好东西，快来买啊";
    }
    return self;
}

/// height: 114
- (void)setupUI {
    self.markButton = ({
        UIButton *tmpBtn = [[UIButton alloc] init];
        [tmpBtn addTarget:self action:@selector(markItemAction:) forControlEvents:UIControlEventTouchUpInside];
        [tmpBtn setImage:[UIImage imageNamed:@"未选中"] forState:UIControlStateNormal];
        [tmpBtn setImage:[UIImage imageNamed:@"选中"] forState:UIControlStateSelected];
        tmpBtn.backgroundColor = [UIColor whiteColor];
        tmpBtn;
    });
    [self addSubview:self.markButton];
    [self.markButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.mas_equalTo(self);
        make.width.mas_equalTo(40);
    }];
    
    self.coverImgView = ({
        UIImageView *tmpImgView = [[UIImageView alloc] init];
        tmpImgView.contentMode = UIViewContentModeScaleAspectFill;
        tmpImgView.image = [UIImage imageNamed:@""];
        tmpImgView.layer.cornerRadius = 5;
        tmpImgView.layer.borderWidth = 0.6;
        tmpImgView.layer.borderColor = [UIColor colorWithRed:213/255.0 green:213/255.0 blue:213/255.0 alpha:1].CGColor;
        tmpImgView;
    });
    self.coverImgView.backgroundColor = [UIColor lightTextColor];
    [self addSubview:self.coverImgView];
    [self.coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(40);
        make.centerY.equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(95, 95));
    }];
    
    self.titleLabel = ({
        UILabel *tmpLabel = [[UILabel alloc] init];
        tmpLabel.textColor = ShoppingCarBlackColor;
        tmpLabel.font = [UIFont systemFontOfSize:15];
        tmpLabel.numberOfLines = 2;
        tmpLabel.textAlignment = NSTextAlignmentLeft;
        tmpLabel;
    });
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.coverImgView.mas_right).offset(8);
        make.top.mas_equalTo(self.coverImgView);
        make.right.equalTo(self.mas_right).offset(-15);
        make.height.mas_equalTo(30);
    }];
    
    self.subTitleLabel = ({
        UILabel *tmpLabel = [[UILabel alloc] init];
        tmpLabel.textColor =  ShoppingCarGrayColor;
        tmpLabel.font = [UIFont systemFontOfSize:13];
        tmpLabel.numberOfLines = 1;
        tmpLabel.textAlignment = NSTextAlignmentLeft;
        tmpLabel;
    });
    [self addSubview:self.subTitleLabel];
    [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.coverImgView.mas_right).offset(8);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(8);
        make.right.equalTo(self.mas_right).offset(-15);
        make.height.mas_equalTo(15);
    }];

    self.priceLabel = ({
        UILabel *tmpLabel = [[UILabel alloc] init];
        tmpLabel.textColor =  ShoppingCarRedColor;
        tmpLabel.font = [UIFont systemFontOfSize:11];
        tmpLabel.numberOfLines = 1;
        tmpLabel.textAlignment = NSTextAlignmentLeft;
        tmpLabel;
    });
    [self addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.coverImgView.mas_right).offset(8);
        make.right.equalTo(self.mas_right).offset(-15);
        make.bottom.equalTo(self.coverImgView.mas_bottom);
        make.height.mas_equalTo(15);
    }];
    
    self.amountView = [ShoppingInfoEditAmountView new];
    [self addSubview:self.amountView];
    [self.amountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom).offset(-10);
        make.right.equalTo(self.mas_right).offset(-15);
        make.size.mas_equalTo(CGSizeMake(80, 22));
    }];
    self.amountView.max = 200;
    self.amountView.changeShoppingCount = ^(ShoppingCountChangeStyle style, NSInteger currentValue) {
        NSLog(@"%ld-%ld",(long)style,currentValue);
    };
}

#pragma mark - event response

- (void)markItemAction:(UIButton *)sender {
    sender.selected = !sender.isSelected;
}

#pragma mark - private methods
- (void)setAttributedText:(CGFloat)money {
    self.priceLabel.attributedText = [NSString  differentFontWithMoney:money moneyFont:nil integerFont:[UIFont systemFontOfSize:13] decimalPointFont:[UIFont systemFontOfSize:11]];
}
#pragma mark - getters and setters

@end
