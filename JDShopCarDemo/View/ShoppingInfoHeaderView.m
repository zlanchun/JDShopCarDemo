//
//  ShoppingInfoHeaderView.m
//  ShoppingCar
//
//  Created by z on 2017/7/11.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "ShoppingInfoHeaderView.h"
#import "ShoppingCarHeader.h"

@interface ShoppingInfoHeaderView()<UITextFieldDelegate>
@property (nonatomic,strong) UIButton *markButton;
@property (nonatomic,strong) UILabel *titleLabel;
@end

@implementation ShoppingInfoHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI {
    self.backgroundColor = [UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1];
    self.markButton = ({
        UIButton *tmpBtn = [[UIButton alloc] init];
        [tmpBtn addTarget:self action:@selector(markItemAction:) forControlEvents:UIControlEventTouchUpInside];
        [tmpBtn setImage:[UIImage imageNamed:@"未选中"] forState:UIControlStateNormal];
        [tmpBtn setImage:[UIImage imageNamed:@"选中"] forState:UIControlStateSelected];
        tmpBtn.backgroundColor = [UIColor clearColor];
        tmpBtn;
    });
    [self addSubview:self.markButton];
    [self.markButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.mas_equalTo(self);
        make.width.mas_equalTo(40);
    }];
    
    self.titleLabel = ({
        UILabel *tmpLabel = [[UILabel alloc] init];
        tmpLabel.textColor = ShoppingCarBlackColor;
        tmpLabel.font = [UIFont systemFontOfSize:15];
        tmpLabel.numberOfLines = 2;
        tmpLabel.textAlignment = NSTextAlignmentLeft;
        tmpLabel;
    });
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(40);
        make.right.equalTo(self.mas_right).offset(-15);
        make.top.bottom.mas_equalTo(self);
    }];
}

- (void)markItemAction:(UIButton *)sender {
    sender.selected = !sender.isSelected;
}

- (void)setTitle:(NSString *)title {
    _title = [title copy];
    self.titleLabel.text = _title;
}
@end
