//
//  ShoppingInfoEditAmountView.m
//  ShoppingCar
//
//  Created by z on 2017/7/11.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "ShoppingInfoEditAmountView.h"
#import "ShoppingCarHeader.h"

#define ShoppingInfoEditAmountViewDefaultWidth 0.8f

@interface ShoppingInfoEditAmountView()<UITextFieldDelegate>
@property (nonatomic,strong) UIButton *decreaseButton;
@property (nonatomic,strong) UIButton *increaseButton;
@property (nonatomic,strong) UITextField *amountTF;
@property (nonatomic,assign) NSInteger count;
@end

@implementation ShoppingInfoEditAmountView

#pragma mark - life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initData];
        [self setupUI];
        [self configureNotification];
    }
    return self;
}

- (void)initData {
    self.count = 1;
}

- (void)setupUI {
    self.backgroundColor = [UIColor whiteColor];
    self.clipsToBounds = YES;
    self.layer.borderColor = ShoppingCarGrayColor.CGColor;
    self.layer.borderWidth = ShoppingInfoEditAmountViewDefaultWidth;
    self.layer.cornerRadius = 3.0f;
    
    self.decreaseButton = ({
        UIButton *tmpBtn = [[UIButton alloc] init];
        [tmpBtn addTarget:self action:@selector(decreaseAction:) forControlEvents:UIControlEventTouchUpInside];
        [tmpBtn setTitle:@"一" forState:UIControlStateNormal];
        [tmpBtn setTitleColor:ShoppingCarGrayColor forState:UIControlStateNormal];
        tmpBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        tmpBtn.backgroundColor = [UIColor whiteColor];
        tmpBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        tmpBtn.layer.borderColor = ShoppingCarGrayColor.CGColor;
        tmpBtn.layer.borderWidth = ShoppingInfoEditAmountViewDefaultWidth;
        tmpBtn;
    });
    [self addSubview:self.decreaseButton];
    [self.decreaseButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.mas_equalTo(self);
        make.width.mas_equalTo(22);
    }];
    
    self.increaseButton = ({
        UIButton *tmpBtn = [[UIButton alloc] init];
        [tmpBtn addTarget:self action:@selector(increaseAction:) forControlEvents:UIControlEventTouchUpInside];
        [tmpBtn setTitle:@"+" forState:UIControlStateNormal];
        [tmpBtn setTitleColor:ShoppingCarBlackColor forState:UIControlStateNormal];
        tmpBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        tmpBtn.backgroundColor = [UIColor whiteColor];
        tmpBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        tmpBtn.layer.borderColor = ShoppingCarGrayColor.CGColor;
        tmpBtn.layer.borderWidth = ShoppingInfoEditAmountViewDefaultWidth;
        tmpBtn;
    });
    [self addSubview:self.increaseButton];
    [self.increaseButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.mas_equalTo(self);
        make.width.mas_equalTo(22);
    }];
    
    self.amountTF =  ({
        UITextField *tmpTF = [[UITextField alloc] init];
        tmpTF.font = [UIFont systemFontOfSize:13];
        tmpTF.textColor = ShoppingCarBlackColor;
        tmpTF.borderStyle = UITextBorderStyleNone;
        tmpTF.text = @"1";
        tmpTF.textAlignment = NSTextAlignmentCenter;
        tmpTF.keyboardType = UIKeyboardTypeASCIICapableNumberPad;
        tmpTF.clearButtonMode = UITextFieldViewModeNever;
        tmpTF.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        tmpTF.clipsToBounds = YES;
        tmpTF;
    });
    self.amountTF.delegate = self;
    [self addSubview:self.amountTF];
    [self.amountTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 22, 0, 22));
    }];
}

- (void)configureNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChanged:) name:UITextFieldTextDidChangeNotification object:nil];
}

#pragma mark - textfield delegate
- (void)textFieldTextDidChanged:(NSNotification *)notification {
    UITextField *tf = (UITextField *)notification.object;
    if (tf == self.amountTF ) {
        NSInteger currentCount = tf.text.integerValue;
        self.count = currentCount;
        if (self.max != 0 && currentCount > self.max) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                self.amountTF.text = [NSString stringWithFormat:@"%ld",self.max];
                self.count = self.max;
            });
        } else if (self.changeShoppingCount) {
            self.changeShoppingCount(ShoppingCountChangedByTextfield, self.count);
        }
    }
}

#pragma mark - event response
- (void)decreaseAction:(UIButton *)sender {
    if (self.count == 1) { return; }
    self.count -= 1;
    if (self.changeShoppingCount) {
        self.changeShoppingCount(ShoppingCountDecrease, self.count);
    }
}

- (void)increaseAction:(UIButton *)sender {
    if (self.max != 0 && self.count == self.max) { return; }
    self.count += 1;
    if (self.changeShoppingCount) {
        self.changeShoppingCount(ShoppingCountIncrease, self.count);
    }
}

- (void)endEditing {
    [self.amountTF endEditing:YES];
}

#pragma mark - private methods

#pragma mark - getters and setters
- (void)setCount:(NSInteger)count {
    _count = count;
    if (count == 1) {
        [self.decreaseButton setTitleColor:ShoppingCarGrayColor forState:UIControlStateNormal];
    } else {
        [self.decreaseButton setTitleColor:ShoppingCarBlackColor forState:UIControlStateNormal];
    }
    self.amountTF.text = [NSString stringWithFormat:@"%ld",count];
}

- (void)setBorderColor:(UIColor *)borderColor {
    _borderColor = borderColor;
    self.layer.borderColor = _borderColor.CGColor;
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    _borderWidth = borderWidth;
    self.layer.borderWidth = borderWidth;
}

@end
