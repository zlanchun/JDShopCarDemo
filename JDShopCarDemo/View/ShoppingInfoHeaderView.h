//
//  ShoppingInfoHeaderView.h
//  ShoppingCar
//
//  Created by z on 2017/7/11.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingInfoHeaderView : UIView
@property (nonatomic,copy) NSString *title;
@end
