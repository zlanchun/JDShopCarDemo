//
//  HomeViewController.m
//  JDShopCarDemo
//
//  Created by z on 2017/7/11.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "HomeViewController.h"
#import "ShoppingInfoCell.h"
#import "ShoppingInfoHeaderView.h"
#import "ShoppingInforRcommendController.h"

#import "ShoppingRecommendCell.h"

@interface HomeViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,assign) CGSize tableViewContentSize;
@end

@implementation HomeViewController

#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:239/255.0 green:242/255.0 blue:247/255.0 alpha:1];
    self.title = @"JDShopCar";
    [self configureTableView];
    [self configureCollectionView];
}

#pragma mark - UICollectionViewDelegate 
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == 0) { return 1; }
    return 100;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UICollectionViewCell" forIndexPath:indexPath];
        [cell addSubview:self.tableView];
        self.tableView.frame = cell.frame;
        return cell;
    }
    
    ShoppingRecommendCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ShoppingRecommendCell" forIndexPath:indexPath];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        return self.tableViewContentSize;
    }
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGSize size =  CGSizeMake((width - 2 * 1) / 2.0, ((width - 2 * 1) / 2.0)/0.70);
    return size;
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 5;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ShoppingInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ShoppingInfoCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setAttributedText:20.33];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    ShoppingInfoHeaderView *header =  [ShoppingInfoHeaderView new];
    header.title = @"京东自营";
    if  (section != 0) {
        header.title = @"monqiqi旗舰店";
    }
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 112;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

#pragma mark - CustomDelegate

#pragma mark - event response
- (void)configureTableView {
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor colorWithRed:239/255.0 green:242/255.0 blue:247/255.0 alpha:1];
    //注意，这里不添加 tableView
//    [self.view addSubview:self.tableView];
    [self.tableView registerClass:[ShoppingInfoCell class] forCellReuseIdentifier:@"ShoppingInfoCell"];
    self.tableView.tableFooterView = [UIView new];
    //获取内容的实际大小：contentSize, 用于计算 collection section0 的大小
    //正式环境的话，应该是在 reloadData 里刷新 tableView reload 之后进行复制
    self.tableViewContentSize = self.tableView.contentSize;
}

- (void)configureCollectionView {
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
    [self.collectionView registerClass:[ShoppingRecommendCell class] forCellWithReuseIdentifier:@"ShoppingRecommendCell"];
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"UICollectionViewCell"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
}

#pragma mark - private methods

#pragma mark - getters and setters
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    }
    return _tableView;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        CGFloat space = 2;

        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        flowLayout.minimumLineSpacing = space;
        flowLayout.minimumInteritemSpacing = space;
        flowLayout.sectionInset = UIEdgeInsetsMake(space, 0, space, 0);
        
        
        _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
    }
    return _collectionView;
}


#pragma mark - receive memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
