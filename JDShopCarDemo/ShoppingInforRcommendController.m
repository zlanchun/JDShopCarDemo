//
//  ShoppingInforRcommendController.m
//  ShoppingCar
//
//  Created by z on 2017/7/11.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "ShoppingInforRcommendController.h"

@interface ShoppingInforRcommendController ()

@end

@implementation ShoppingInforRcommendController

#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor redColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark - UITableViewDelegate

#pragma mark - CustomDelegate

#pragma mark - event response

#pragma mark - private methods

#pragma mark - getters and setters

#pragma mark - receive memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
